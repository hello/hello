# hello

A simple program that implements the "Hello World" example in C++ using the
`libhello` library. Its primary goal is to show a canonical `build2`/`bpkg`
project/package.
