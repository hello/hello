// file: hello.cxx -*- C++ -*-

import std.core;
import std.io;
import hello;

using namespace std;

int
main (int argc, char* argv[])
{
  if (argc < 2)
  {
    cerr << "usage: hello <name>..." << endl;
    return 1;
  }

  using hello::say;

  for (int i (1); i != argc; ++i)
    say (argv[i]);
}
